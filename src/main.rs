// Copyright 2023 Matthew James Kraai

// This file is part of gtd.

// gtd is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.

// gtd is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
// details.

// You should have received a copy of the GNU Affero General Public License
// along with gtd.  If not, see <https://www.gnu.org/licenses/>.

use clap::Parser;
use directories::ProjectDirs;
use rusqlite::Connection;
use std::{fs::DirBuilder, os::unix::fs::DirBuilderExt};

#[derive(Parser)]
enum Args {
    /// Add an item to the inbox
    Add {
        /// Description of the item
        #[arg(required = true)]
        description: Vec<String>,
    },
}

fn main() {
    let args = Args::parse();
    let project_dirs = ProjectDirs::from("org.ftbfs", "", "gtd").unwrap();
    let data_dir = project_dirs.data_dir();
    DirBuilder::new()
        .recursive(true)
        .mode(0o700)
        .create(data_dir)
        .unwrap();
    let connection = Connection::open(data_dir.join("database.sqlite3")).unwrap();
    connection
        .execute(
            "CREATE TABLE IF NOT EXISTS inbox (description TEXT NOT NULL PRIMARY KEY)",
            [],
        )
        .unwrap();
    match args {
        Args::Add { description } => {
            let description = description.join(" ");
            connection
                .execute(
                    "INSERT INTO inbox VALUES(:description)",
                    &[(":description", &description)],
                )
                .unwrap();
        }
    }
}

// Copyright 2023 Matthew James Kraai

// This file is part of gtd.

// gtd is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.

// gtd is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
// details.

// You should have received a copy of the GNU Affero General Public License
// along with gtd.  If not, see <https://www.gnu.org/licenses/>.

use assert_cmd::prelude::*;
use predicates::prelude::*;
use std::process::Command;

#[test]
fn add() -> Result<(), Box<dyn std::error::Error>> {
    let dir = tempfile::tempdir().unwrap();

    let mut cmd = Command::cargo_bin("gtd")?;
    cmd.env("HOME", dir.path()).arg("add").arg("Develop gtd.");
    cmd.assert().success().stdout(predicate::eq(""));

    Ok(())
}
